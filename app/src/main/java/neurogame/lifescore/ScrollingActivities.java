package neurogame.lifescore;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class ScrollingActivities extends AppCompatActivity {

    final static String ACTIVITIES_FILE_NAME = "lifescore_activities.dat";
    final static String SCORE_HISTORY_FILE_NAME = "lifescore_score_history.dat";
    final static String ACTIVITY_HISTORY_FILE_NAME = "lifescore_activity_history.dat";
    final static String PREFS_NAME = "lifescore_pref";
    final static String PREF_LAST_WEEK = "last_week";
    final static String PREF_SCORE = "score";
    final Handler handler = new Handler();
    List<Pair<ToggleButton, LifeActivity>> activities = new CopyOnWriteArrayList<>();// ArrayList<Pair<ToggleButton, LifeActivity>>();
    Map<ToggleButton, LifeActivity> btnActivityMap = new HashMap<ToggleButton, LifeActivity>();
    ArrayList<Float> scoreHistory = new ArrayList<>();
    ArrayList<Pair<LifeActivity, Long>> activityHistory = new ArrayList<>();
    long lastWeek = 0L;
    float score = 0;
    boolean weekEnded = false;

    LinearLayout activitiesLayout = null;
    TextView scoreView = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling_activities);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                if (weekEnded) {
                                    weekEndUpdate();
                                    weekEnded = false;
                                }
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                if (weekEnded) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                } else {
                    // yesterday self video recording
                    dispatchTakeVideoIntent();
                }
            }
        });

        // load elements
        activitiesLayout = (LinearLayout) findViewById(R.id.activities_layout);
        scoreView = (TextView) findViewById(R.id.scoreView);

        // load activities
        List<LifeActivity> activityList = deserialize(ACTIVITIES_FILE_NAME, initiateActivities());

        // add extra activities if they don't exist in the list
        List<LifeActivity> extraActivities = addExtraActivities();
        for (final LifeActivity ela : extraActivities) {
            boolean add = true;
            for (final LifeActivity la : activityList) {
                if (la.name.equals(ela.name)) {
                    add = false;
                    break;
                }
            }
            if (add) activityList.add(ela);
        }

//        // edit any activity
//        LifeActivity editedLa = new LifeActivity("Project Work", 40, 3600, 10, -3);
//        for (int i = 0; i < activityList.size(); i++) {
//            if (activityList.get(i).name.equals(editedLa.name)) {
//                //activityList.remove(i); // LOOK OUT !
//                activityList.set(i, editedLa);
//                break;
//            }
//        }

        // really load activities
        for (LifeActivity activity : activityList) {
            final ToggleButton tbtn = new ToggleButton(activitiesLayout.getContext());
            tbtn.setTextOff(activity.toString());
            tbtn.setTextOn(activity.toString());
            tbtn.setText(activity.toString());
            tbtn.setChecked(activity.doing);
            btnActivityMap.put(tbtn, activity);
            tbtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    btnActivityMap.get(tbtn).toggle(isChecked);
                }
            });
            tbtn.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    LifeActivity act = btnActivityMap.get(tbtn);
                    act.done_before_pause = act.duration - 10; // 10 sec before done
                    if (act.doing) act.toggle(false);
                    return false;
                }
            });
            activities.add(new Pair<ToggleButton, LifeActivity>(tbtn, activity));
            activitiesLayout.addView(tbtn);
        }
        reorderActivities();
        saveActivities();

        // load last week time
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        lastWeek = prefs.getLong(PREF_LAST_WEEK, 0);
        if (lastWeek == 0) {
            lastWeek = 1564353564L; // System.currentTimeMillis() / 1000; // FIXME
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong(PREF_LAST_WEEK, lastWeek);
            editor.apply();
        }

        // load score
        score = prefs.getFloat(PREF_SCORE, 1000000); // never gonna get that high
        if (score == 1000000) {
            score = 0;
            SharedPreferences.Editor editor = prefs.edit();
            editor.putFloat(PREF_SCORE, score);
            editor.apply();
        }

        // load score and activityhistory
        scoreHistory = deserialize(SCORE_HISTORY_FILE_NAME, new ArrayList<Float>());
        activityHistory = deserialize(ACTIVITY_HISTORY_FILE_NAME, new ArrayList<Pair<LifeActivity, Long>>());
        performTemporalChanges();

        // plot history of weekly scores
        GraphView graph = (GraphView) findViewById(R.id.graph);
        DataPoint[] dataPoints = new DataPoint[scoreHistory.size() + 1];
        int i = 0;
        for (; i < scoreHistory.size(); ++i)
            dataPoints[i] = new DataPoint(i, scoreHistory.get(i));
        dataPoints[i] = new DataPoint(i, score);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPoints);
        PointsGraphSeries<DataPoint> predicted_score = new PointsGraphSeries<>(
                new DataPoint[]{ new DataPoint(i, score + computeCurrentPunishment()) });
        graph.addSeries(series);
        graph.addSeries(predicted_score);

        // set graph limits
        graph.getViewport().setMinX(dataPoints.length - 25 < 0 ? 0 : dataPoints.length - 25); // 25 weeks
        graph.getViewport().setMaxX(dataPoints.length);
        graph.getViewport().setXAxisBoundsManual(true);

        // start loop
        mainLoop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        for (Pair<ToggleButton, LifeActivity> act : activities) {
            if (act.second.doing)
                act.first.setChecked(true);
        }
    }

    static ArrayList<LifeActivity> addExtraActivities() {
        ArrayList<LifeActivity> extraActivities = new ArrayList<LifeActivity>();
        //extraActivities.add(new LifeActivity("Attend Course", 3, 3600, 15, -10));
        return extraActivities;
    }

    void performTemporalChanges() {
//        lastWeek = 1456697087;
//        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();
//        editor.putLong(PREF_LAST_WEEK, lastWeek);
//        editor.apply();
    }

    static ArrayList<LifeActivity> initiateActivities() {
        ArrayList<LifeActivity> initial_activities = new ArrayList<LifeActivity>();
        //initial_activities.add(new LifeActivity("Lecture", 6, 5400, 15, -5));
        initial_activities.add(new LifeActivity("Special occasion", 0, 0, 100, 0));
        initial_activities.add(new LifeActivity("Smoking", 3, 0, -50, 0));
        initial_activities.add(new LifeActivity("Drinking", 1, 0, -50, 0));
        initial_activities.add(new LifeActivity("Project Work", 50, 3600, 10, -3));
        //initial_activities.add(new LifeActivity("Studying", 8, 3600, 10, -3));
        //initial_activities.add(new LifeActivity("LA PW/Study", 4, 3600, 8, 0));
        initial_activities.add(new LifeActivity("DREAM ON", 0, 420, 1000, 0));
        initial_activities.add(new LifeActivity("Read", 14, 1800, 10, -5));
        initial_activities.add(new LifeActivity("Meditation", 7, 1200, 10, -5));
        initial_activities.add(new LifeActivity("Exercise", 2, 3600, 20, -5));
        //initial_activities.add(new LifeActivity("Windsurf", 1, 7200, 50, -10));
        initial_activities.add(new LifeActivity("Coursework", 2, 3600, 15, -10));
        return initial_activities;
    }

    void mainLoop() {
        // check on activities
        for (Pair<ToggleButton, LifeActivity> act : activities) {
            if (act.second.check()) {
                activityEndUpdate(act.second);
                act.first.setChecked(act.second.doing);
            }
            act.first.setTextOff(act.second.toString());
            act.first.setTextOn(act.second.toString());
            act.first.setText(act.second.toString());
        }

        // update score view
        scoreView.setText(String.format("%.1f", score) + "p / " +
                String.format("%.1f", 7f - (System.currentTimeMillis() / 1000f - lastWeek) / 86400f) + "d");

        // new week
        if (!weekEnded && (System.currentTimeMillis() / 1000 - lastWeek) / 604800 > 0) {
            weekEnded = true;
            showNotification("LifeScore Week End", "SHOW ME WHAT YOU GOT!");
        }

        // delayed call
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mainLoop();
            }
        }, 1000);
    }

    float computeCurrentPunishment() { // of activities done on lower freq than required
        float punishment = 0f;
        for (Pair<ToggleButton, LifeActivity> act : activities) // TODO more sophisticated punishments
            if (act.second.freq - act.second.instances > 0)
                punishment += (act.second.freq - act.second.instances) * act.second.punishment;
        return punishment;
    }

    void weekEndUpdate() {
        playNotificationSound();

        // stop running activities & propagate them for the next week & register punishments
        float punishment = 0f;
        for (Pair<ToggleButton, LifeActivity> act : activities) { // TODO more sophisticated punishments
            if (act.first.isChecked()) act.first.setChecked(false);
            if (act.second.freq - act.second.instances > 0)
                punishment += (act.second.freq - act.second.instances) * act.second.punishment; // punishment can only be negative
            act.second.nextWeek();
        }

        // statistics
        String stat = "Weekend stats: " + score + " points & " + punishment + " punishment.";
        if (!scoreHistory.isEmpty()) {
            stat += " Improvements since last week: " + (score - scoreHistory.get(scoreHistory.size() - 1)) + ".";
            float lessCount = 0;
            for (float s : scoreHistory) lessCount += (s < score) ? 1 : 0;
            stat += " Score is higher than " + (lessCount / scoreHistory.size() * 100) + "% of previous weeks'!";
        }
//        Toast.makeText(getApplicationContext(), stat, Toast.LENGTH_LONG).show();
//        showNotification("Week ended, bitch!", stat);
        new AlertDialog.Builder(this)
                .setMessage(stat).setPositiveButton("OK", null).show();

        // update score history
        scoreHistory.add(score + punishment); // punishment is negative
        serialize(scoreHistory, SCORE_HISTORY_FILE_NAME);

        // save prefs
        score = 0;
        lastWeek = lastWeek + 3600 * 24 * 7; // +1 week
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(PREF_SCORE, score);
        editor.putLong(PREF_LAST_WEEK, lastWeek);
        editor.apply();
    }

    void showNotification(String title, String text) {
        Notification notification = new NotificationCompat.Builder(this)
                .setTicker(title)
                .setSmallIcon(android.R.drawable.ic_menu_report_image)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    void playNotificationSound() {
        // play notification sound
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void activityEndUpdate(LifeActivity activity) {
        playNotificationSound();
        score += activity.score;
        activityHistory.add(new Pair<LifeActivity, Long>(new LifeActivity(activity),
                System.currentTimeMillis() / 1000L));
        serialize(activityHistory, ACTIVITY_HISTORY_FILE_NAME);

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(PREF_SCORE, score);
        editor.apply();
        reorderActivities();
        saveActivities();
    }

    void reorderActivities() {

        // sort according to http://stackoverflow.com/questions/28805283/how-to-sort-copyonwritearraylist
        Object[] array = activities.toArray();
        Arrays.sort(array, new BtnActivityComparator());
        for (int i = 0; i < array.length; ++i)
            activities.set(i,  (Pair<ToggleButton, LifeActivity>)array[i]);

        //Collections.sort(activities, new BtnActivityComparator());
        activitiesLayout.removeAllViews();
        for (Pair<ToggleButton, LifeActivity> act : activities) {
            activitiesLayout.addView(act.first);
        }
    }

    @Override
    protected void onStop() {
        //saveActivities();
        //Log.e("yooooooooooooo", "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        saveActivities();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //saveActivities();
        //Log.e("yooooooooooooo", "onDestroy");
        super.onDestroy();
    }

    void saveActivities() {
        ArrayList<LifeActivity> activityList = new ArrayList<LifeActivity>();
        for (Pair<ToggleButton, LifeActivity> act : activities) {
            //if (act.second.doing) act.second.toggle(false);
            activityList.add(act.second);
            Log.d("save_act", act.second.toString());
        }
        serialize(activityList, ACTIVITIES_FILE_NAME);
    }

    <T extends Serializable>
    boolean serialize(T obj, String filename) {

        // FIXME remove: save publicly
        /*File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, filename);

        FileOutputStream fout2 = null;
        ObjectOutputStream oout2 = null;
        try {
            fout2 = new FileOutputStream(file);
            oout2 = new ObjectOutputStream(fout2);
            oout2.writeObject(obj);
            oout2.close();
            fout2.close();
        } catch (Exception e) {
            Log.e("saving error", e.toString());
            return false;
        }*/

        FileOutputStream fout = null;
        ObjectOutputStream oout = null;
        try {
            fout = openFileOutput(filename, Context.MODE_PRIVATE);
            oout = new ObjectOutputStream(fout);
            oout.writeObject(obj);
            oout.close();
            fout.close();
        } catch (Exception e) {
            Log.e("saving error", e.toString());
            return false;
        }
        return true;
    }

    <T extends Serializable>
    T deserialize(String filename, T defval) {
        T obj = null;
        FileInputStream fin = null;
        ObjectInputStream oin = null;
        try {
            fin = openFileInput(filename);
            oin = new ObjectInputStream(fin);
            obj = (T) oin.readObject();
            oin.close();
            fin.close();
        } catch (Exception e) {
            Log.e("loading error", e.toString());

            obj = defval;
        }
        return obj;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling_activities, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class BtnActivityComparator implements Comparator<Object> { // object due to CopyOnWriteList shit...
        @Override
        public int compare(Object lhs, Object rhs) {
            Pair<ToggleButton, LifeActivity> lhsPair = (Pair<ToggleButton, LifeActivity>) lhs;
            Pair<ToggleButton, LifeActivity> rhsPair = (Pair<ToggleButton, LifeActivity>) rhs;
            return lhsPair.second.compareTo(rhsPair.second);
        }
    }

    //////////////// YESTERDAY SELF ////////////////
    final static int REQUEST_VIDEO_CAPTURE = 1;

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    TimePickerDialog mTimePicker;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            final Uri videoUri = intent.getData();
            Toast.makeText(getApplicationContext(), videoUri.getPath(), Toast.LENGTH_SHORT).show();

            final Calendar selectedTime = Calendar.getInstance();
            int hour = selectedTime.get(Calendar.HOUR_OF_DAY);
            int minute = selectedTime.get(Calendar.MINUTE);
            TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    selectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    selectedTime.set(Calendar.MINUTE, minute);

                    if (selectedTime.before(Calendar.getInstance()))
                        selectedTime.add(Calendar.HOUR_OF_DAY, 24); // add 1 day if before

                    Intent intent = new Intent(ScrollingActivities.this, ScrollingActivities.AlarmReceiver.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("VIDEO_URI", videoUri.toString());
                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(ScrollingActivities.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

                    // get the alarm manager, and schedule an alarm that calls the receiver
                    ((AlarmManager) getSystemService(ALARM_SERVICE)).set(
                            AlarmManager.RTC, selectedTime.getTimeInMillis(), pendingIntent);
                    Toast.makeText(ScrollingActivities.this,
                            "Timer set to " + selectedTime.get(Calendar.HOUR_OF_DAY) + ":" + selectedTime.get(Calendar.MINUTE),
                            Toast.LENGTH_SHORT).show();
                }
            };

            mTimePicker = new TimePickerDialog(ScrollingActivities.this, onTimeSetListener, hour, minute, true);
            mTimePicker.setTitle("Select Alarm Time");
            mTimePicker.show();
        }
    }

    public static class AlarmReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Intent playVidIntent = new Intent(context, VideoPlayerActivity.class); // TODO video
            playVidIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            playVidIntent.putExtra("VIDEO_URI", intent.getStringExtra("VIDEO_URI"));
            context.startActivity(playVidIntent);
        }
    }

}
