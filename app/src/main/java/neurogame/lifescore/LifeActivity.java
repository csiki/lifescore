package neurogame.lifescore;

import android.text.format.Time;
import android.util.Log;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Viktor on 2/7/2016.
 */
public class LifeActivity implements Serializable, Comparable {

    public String name = "";
    public int freq = 0; // 1/week
    public int duration = 0; // sec
    public float score = 0f;
    public float punishment = 0f; // if instances < freq at the weekend

    public int instances = 0; // in the current week
    public boolean doing = false;
    public long done_before_pause = 0; // sec; if paused, store currTime-started
    public long started = 0;

    public boolean finished = false;

    public LifeActivity() {}

    public LifeActivity(String name, int freq, int duration, float score, float punishment) {
        this.name = name;
        this.freq = freq;
        this.duration = duration;
        this.score = score;
        this.punishment = punishment;
    }

    public LifeActivity(LifeActivity act) {
        this(act.name, act.freq, act.duration, act.score, act.punishment);
        this.instances = act.instances;
        this.doing = act.doing;
        this.done_before_pause = act.done_before_pause;
        this.started = act.started;
        this.finished = act.finished;
    }

    public void nextWeek() {
        instances = 0;
        doing = false;
        done_before_pause = 0;
        started = 0;
    }

    public void toggle(boolean isChecked) {
        if (finished && !isChecked) // just to catch the toggle() call when setting the btn unchecked after finishing
            finished = false;
        else if (finished && isChecked && !doing) {
            started = System.currentTimeMillis() / 1000;
            doing = true;
        }
        else if (doing && !isChecked) { // pause
            done_before_pause += System.currentTimeMillis() / 1000 - started;
            doing = false;
        } else if (!doing && isChecked) { // resume or start over
            started = System.currentTimeMillis() / 1000;
            doing = true;
        }
    }

    public boolean check() {
        if (doing && duration < (System.currentTimeMillis() / 1000 - started + done_before_pause)) { // finished
            doing = false;
            done_before_pause = 0;
            ++instances;
            finished = true;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        long timeLeft = (duration - (System.currentTimeMillis() / 1000 - started + done_before_pause));
        if (!doing && done_before_pause == 0) // not started (nor paused)
            timeLeft = duration;
        else if (!doing)
            timeLeft = duration - done_before_pause;
        int inst_left = freq - instances; // (freq - instances) > 0 ? (freq - instances) : 0;
        return name + " | " + score + "p | " + inst_left + " left | " + timeLeft + "s";
    }

    @Override
    public int compareTo(Object another) {
        LifeActivity otherActivity = (LifeActivity) another;
        return (otherActivity.freq - otherActivity.instances) - (freq - instances);
    }
}
